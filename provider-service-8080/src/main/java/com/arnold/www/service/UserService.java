package com.arnold.www.service;

import com.arnold.www.pojo.User;

public interface UserService {

    User checkLogin(User user);

    User breaker(Integer id);

}
