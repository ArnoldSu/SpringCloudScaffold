package com.arnold.www.common;


import java.io.Serializable;

/**
 * @ClassName CommonResult
 * @Description:统一回参封装
 * @Author Arnold
 * @Date 2020/4/26
 * @Version V2.0
 **/
public class CommonResult<T> implements Serializable {
    private Integer code;
    private String message;
    private T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public CommonResult(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public CommonResult() {

    }
}