package com.arnold.www;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @ClassName ProviderService8080Application
 * @Description: 启动类
 * @Author Arnold
 * @Date 2020/4/26
 * @Version V2.0
 **/
@SpringBootApplication
@EnableEurekaClient
public class GateWayApplication {


    public static void main(String[] args) {
        SpringApplication.run(GateWayApplication.class, args);
    }

}