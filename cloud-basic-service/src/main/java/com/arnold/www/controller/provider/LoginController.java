package com.arnold.www.controller.provider;

import com.arnold.www.common.CommonResult;
import com.arnold.www.pojo.User;
import com.arnold.www.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName LoginController
 * @Description: 登陆控制器
 * @Author Arnold
 * @Date 2020/4/15
 * @Version V2.0
 **/
@RestController
@Api(tags = "登陆")
public class LoginController {
    @Autowired
    UserService userService;


    @ApiOperation(value = "登陆校验", notes = "通过用户名和密码校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "用户名", paramType = "querry", required = true, dataType = "string"),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "querry", required = true, dataType = "string"),

    })
    @PostMapping(value = "/login")
    public CommonResult<User> checkLogin(@RequestBody User user) {
        User result = userService.checkLogin(user);
        return new CommonResult<User>(200, "成功", result);
    }

    @PostMapping(value = "/timeout")
    public CommonResult<User> timeout(@RequestBody User user) {
        //模拟服务器异常，手动造成异常
        User result = userService.checkLogin(user);
        return new CommonResult(200, "成功", result);
    }

    @ApiOperation(value = "获取用户信息", notes = "通过用户id获取")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", paramType = "path", dataType = "Integer")
    })
    @GetMapping("/get/{id}")
    public CommonResult<User> getUser(@PathVariable("id") Integer id) {
        return new CommonResult(200, "成功", userService.breaker(id));
    }

}