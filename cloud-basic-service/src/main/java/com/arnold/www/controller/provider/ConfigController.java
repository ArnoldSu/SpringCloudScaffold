package com.arnold.www.controller.provider;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName ConfigController
 * @Description: config控制器
 * @Author Arnold
 * @Date 2020/4/15
 * @Version V2.0
 **/
@Api(tags = "config控制器")
@RestController
@RefreshScope
public class ConfigController {
    @Value("${config.info}")
    private String profile;

    @ApiOperation(value = "获取配置端口信息", notes = "获取配置端口信息")
    @GetMapping("/getConfig")
    public String getPort() {
        return profile;
    }

}