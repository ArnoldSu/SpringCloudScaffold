package com.arnold.www.dao;

import com.arnold.www.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {

    User checkLogin(User user);

    User getUserById(Integer id);
}
