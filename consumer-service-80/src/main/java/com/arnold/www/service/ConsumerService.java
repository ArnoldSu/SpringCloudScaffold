package com.arnold.www.service;

import com.arnold.www.common.CommonResult;
import com.arnold.www.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(value = "PROVIDER-SERVICE-8080", fallback = ConsumerServiceImpl.class)
public interface ConsumerService {

    @PostMapping("/login")
    CommonResult<User> checkLogin(@RequestBody User user);

    @PostMapping("/timeout")
    CommonResult<User> timeout(@RequestBody User user);
}

