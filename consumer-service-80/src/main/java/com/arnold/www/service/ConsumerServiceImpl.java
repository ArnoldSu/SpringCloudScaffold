package com.arnold.www.service;

import com.arnold.www.common.CommonResult;
import com.arnold.www.pojo.User;
import org.springframework.stereotype.Component;

/**
 * @ClassName ConsumerServiceImpl
 * @Description: TODO
 * @Author Arnold
 * @Date 2020/5/4
 * @Version V2.0
 **/
@Component
public class ConsumerServiceImpl implements ConsumerService {
    @Override
    public CommonResult<User> checkLogin(User user) {
        return new CommonResult<User>(200, "checkLogin 方法降级!!!", null);
    }

    @Override
    public CommonResult<User> timeout(User user) {
        return new CommonResult<User>(200, "timeout 方法降级!!!", null);
    }
}