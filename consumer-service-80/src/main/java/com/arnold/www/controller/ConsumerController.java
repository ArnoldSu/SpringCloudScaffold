package com.arnold.www.controller;

import com.arnold.www.common.CommonResult;
import com.arnold.www.pojo.User;
import com.arnold.www.service.ConsumerService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


/**
 * @ClassName ConsumerController
 * @Description: 消费端控制器
 * @Author Arnold
 * @Date 2020/4/29
 * @Version V2.0
 **/
@RestController
//@DefaultProperties(defaultFallback = "fallback")
public class ConsumerController {

    public static final String provider_url = "http://localhost:8080";

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ConsumerService consumerService;

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    /**
     * 传统调用
     */
    @PostMapping("/consumer/login")
    public CommonResult<User> login(User user) {
        return restTemplate.postForObject(provider_url + "/login", user, CommonResult.class);
    }

    /**
     * 传统调用
     */
    @PostMapping("/consumer/feign/login")
    public CommonResult<User> feginLogin(User user) {
        return consumerService.checkLogin(user);
    }

    /**
     * 服务降级
     */
    @PostMapping("/consumer/timeout")
    //模拟服务降级处理
//    @HystrixCommand(fallbackMethod = "fallback",
//            commandProperties = {@HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="3000")})
    //@HystrixCommand
    public CommonResult<User> timeout(User user) {
        return consumerService.timeout(user);
    }

    public CommonResult<User> fallback() {
        return new CommonResult<User>(200, "客户端服务降级", null);
    }

}