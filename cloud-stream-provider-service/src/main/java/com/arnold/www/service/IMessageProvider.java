package com.arnold.www.service;

/**
 * @Auther Carroll
 * @Date 2020/3/26
 * @e-mail ggq_carroll@163.com
 */
public interface IMessageProvider {
    public String send();
}
